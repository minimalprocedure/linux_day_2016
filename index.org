#+TITLE: Coding, ovvero il delirio computazionale
#+AUTHOR: Massimo Maria Ghisalberti - pragmas.org
#+DATE: Linux Day 2016 - (22/10/2016)
#+EMAIL: massimo.ghisalberti@pragmas.org

#+STARTUP: showall 
#+OPTIONS: fnadjust
#+DESCRIPTION: Coding e pensiero computazionale
#+KEYWORDS: Coding, pensiero computazionale, Kojo, Scala, code.org, programmailfuturo.it, open source
#+LANGUAGE: it
#+REVEAL_THEME: black
#+REVEAL_TRANS: default
#+REVEAL_EXTRA_CSS: stylesheet.css
#+REVEAL_ROOT: ./reveal.js
#+REVEAL_PLUGINS: (highlight)
#+REVEAL_PREAMBLE: <div class="preamble"><img class="header-left" src="./images/linux-day-2016-1.png" /><span class="header-left"> Linux Day 2016</span><img class="header-right" src="./images/lug-perugia.png" /><span class="header-right"> GNU/Linux User Group Perugia</span><div class="clearfix"></div></div>

#+REVEAL_POSTAMBLE: <div class="postamble"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">(CC BY-NC-SA 4.0)</a></div>

#+OPTIONS: toc:nil num:nil
#+OPTIONS: ^:t
#+OPTIONS: reveal_progress:nil, reveal_rolling_links:nil, reveal_history:t, reveal_width:1320, reveal_slide_number:h.v

#+REVEAL_MULTIPLEX_ID: 48919b172909092d
#+REVEAL_MULTIPLEX_SECRET: 14763716074322646855
#+REVEAL_MULTIPLEX_URL: https://reveal-js-multiplex-ccjbegmaii.now.sh
#+REVEAL_MULTIPLEX_SOCKETIO_URL: https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.5.0/socket.io.min.js
#+REVEAL_PLUGINS: (highlight multiplex)

#+LATEX_CLASS: nissl-org-article
#+LATEX_CLASS_OPTIONS: [a4paper]
#+LATEX_HEADER:\fancypagestyle{plain}{
#+LATEX_HEADER:  \fancyhf{} 
#+LATEX_HEADER:  \fancyfoot[LO]{\small Pragmas s.n.c – via degli Olivi 12 - 06081 Assisi (loc. Palazzo), Italia \linebreak p.iva:  02660460540 - tel.: +39 3939018290/3939909262 – email: contact.info@pragmas.org }}
#+LATEX_HEADER:  \renewcommand{\headrulewidth}{0pt}
#+LATEX_HEADER:  \renewcommand{\footrulewidth}{0.4pt}
#+LATEX_HEADER:}
#+LATEX_HEADER:\fancypagestyle{fancyplain}{
#+LATEX_HEADER:  \fancyfoot[LO]{\small Pragmas s.n.c – via degli Olivi 12 - 06081 Assisi (loc. Palazzo), Italia  \linebreak p.iva:  02660460540 - tel.: +39 3939018290/3939909262 – email: contact.info@pragmas.org }}
#+LATEX_HEADER:  \renewcommand{\footrulewidth}{0.4pt}
#+LATEX_HEADER:}
#+LATEX_HEADER:\pagestyle{fancyplain}

* 
#+ATTR_HTML: :align right :class logo_big_right
[[file:./images/lug-perugia.png]]

#+ATTR_HTML: :align left :class logo_big_left
[[file:./images/linux-day-2016.png]] 

* Premessa

+ Non sarò breve.
+ Chi ha una connessione ad Internet potrà seguire le slide a questo indirizzo:
  + [[http://minimalprocedure.pragmas.org/writings/LinuxDay2016/index_client.html][http://minimalprocedure.pragmas.org/writings/LinuxDay2016/index_client.html]]

* Qualche parola sul CAD (Codice Amministrazione Digitale)

+ Nel CAD si dice all'art.: [[http://archivio.digitpa.gov.it/cad/analisi-comparativa-delle-soluzioni][68]] che il software _proprietario è l'ultima alternativa_.
+ La scuola pubblica non va dimenticato è una Pubblica Amministrazione.
+ Il MIUR ha fatto accordi con Microsoft per *Minecraft for Education*.
+ *Minecraft for Education* richiede Microsoft Windows^tm 10 o Apple OSX El Capitain ([[http://education.minecraft.net/knowledgebase-category/availability/][http://education.minecraft.net/knowledgebase-category/availability/]]).


** Interrogativi ed affermazioni

+ Perché non Minetest ([[http://www.minetest.net/][http://www.minetest.net/]]) che è Open Source?
+ I nostri ragazzi dovranno diventare futuri clienti delle blasonate software house?
+ La scuola pubblica non deve _fidelizzare_ i nostri ragazzi ad un prodotto commerciale.

* Il coding

+ *coding*: gerundio o participio presente del verbo /to code/.
+ *Codificare* non significa necessariamente programmare i computer.
+ La semantica delle parole sta diventando un'opinione e lo *storytelling* impera.
+ *Storytelling* nel senso che ce la raccontano?
+ Il /coding/ è un _business_?

** Wikipedia in italiano

+ /La programmazione, in informatica, è l'insieme delle _attività_ e _tecniche_ che una o più _persone specializzate_, programmatori o sviluppatori (developer), svolgono per creare un programma, ossia un software da far eseguire ad un computer, scrivendo il relativo codice sorgente in un certo linguaggio di programmazione./

** Wikipedia in inglese

+ /Computer programming (often shortened to programming) is a process that leads from an original formulation of a computing problem to executable computer programs. Programming involves activities such as analysis, developing understanding, generating algorithms, verification of requirements of algorithms including their correctness and resources consumption, and implementation (commonly referred to as _coding_) of algorithms in a target programming language. Source code is written in one or more programming languages. The purpose of programming is to find a sequence of instructions that will automate performing a specific task or solving a given problem. The process of programming thus often requires expertise in many different subjects, including knowledge of the application domain, specialized algorithms and formal logic./

** Definizioni diverse per popoli diversi

+ *italiani*: programmare è un'attività svolta da tecnici (ovvero stregoneria pura).
+ *anglosassoni*: programmare è un /processo cognitivo./
+ *io:* sono d'accordo con gli *anglosassoni* (in questo caso).

* Il *Coding* secondo il MIUR ed i suoi guru: il mantra.

+ Il /coding/ è per tutti.
+ il /coding/ è divertente.
+ il /coding/ è facile.
+ il /coding/ si insegna dopo un corso di poche ore.
+ il /coding/ si insegna con una T-shirt con le scritta: *Coding in...*

** il /coding/ secondo le *persone tristi* e grigie

+ Il /coding/ non è per tutti.
+ Il /coding/ è /anche/ divertente, se si sa cosa fare.
+ Il /coding/ non è così semplice come lo raccontano.
+ Per imparare a fare del /coding/ servono anni di esperienza e studio. 
+ Insistere sul /ludico/ e /facile/ sminuisce il _ruolo_ e la _bellezza_ della *[[https://en.wikipedia.org/wiki/Computer_science][computer science]]*.

** Quindi? Anzi no, /...e quindi?/

+ Questa è spesso la replica alle mie obiezioni sul /coding/.
+ Anzi no, la replica è: /...e quindi?/
+ Qualcuno mi ha detto che: /Stiamo facendo un percorso bellissimo.../
+ Qualcuno ha presentato un confronto tra /Scratch/ ed un codice in /C/, affermando che /Scratch/ _era più semplice_.
+ (/ps: il codice in C era errato/.)
+ Repliche sempre _molto argomentate_.

* Il delirio computazionale

+ Siamo in pieno /delirio computazionale/.
+ I guru del coding spuntano come funghi dopo una pioggia ed un po' di sole.
+ Il successo del /coding/ a scuola sono numeri puri e assoluti (alcuni con la virgola).
+ *unmilioneseicentocinquantasettemilacentouno*. (MIUR [[http://hubmiur.pubblica.istruzione.it/web/ministero/cs150915][http://hubmiur.pubblica.istruzione.it/web/ministero/cs150915]]).
+ *in media 8,5 ore di pensiero computazionale* per partecipante.

* programma il futuro

+ [[http://www.programmailfuturo.it/][http://www.programmailfuturo.it/]]
+ È un progetto del *MIUR* e *CINI* (Consorzio Inter universitario Nazionale per l'Informatica). 

** Descrizione del progetto (21/10/2016)

+ [[http://www.programmailfuturo.it/progetto/descrizione-del-progetto][http://www.programmailfuturo.it/progetto/descrizione-del-progetto]] 

+ /Partendo da un’esperienza di successo avviata negli USA nel 2013 che ha visto sino ad ora la partecipazione di circa 200 milioni di studenti e insegnanti di tutto il mondo, l’Italia è stato uno dei primi Paesi al mondo a sperimentare l’introduzione strutturale nelle scuole dei concetti di base dell’informatica attraverso la programmazione (coding ), usando strumenti di facile utilizzo e che non richiedono un’abilità avanzata nell’uso del computer. L’iniziativa, con la partecipazione nel corso dell'a.s. 2015-16 di oltre 1.000.000 studenti, 15.000 insegnanti e 5.000 scuole in tutta Italia, colloca il nostro Paese all’avanguardia in Europa e nel mondo./

*** Perché sperimentare il coding nelle scuole italiane

+ /Il lato scientifico-culturale dell'informatica, definito anche pensiero computazionale, aiuta a sviluppare competenze logiche e capacità di risolvere problemi in modo creativo ed efficiente, qualità che sono importanti per tutti i futuri cittadini. Il modo più semplice e divertente di sviluppare il pensiero computazionale è attraverso la programmazione (coding ) _in un contesto di gioco_./
+ Più semplice per chi: ragazzi o insegnanti?

*** Gli strumenti a disposizione delle scuole

+ /Gli strumenti disponibili sono di _elevata qualità didattica e scientifica_, progettati e realizzati in modo da renderli utilizzabili in classe da parte di insegnanti di qualunque materia. _Non è necessaria alcuna particolare abilità tecnica né alcuna preparazione scientifica_. Il materiale didattico può essere fruito con successo da tutti i livelli di scuole. Raccomandiamo soprattutto alle scuole primarie di avvicinare i propri studenti allo sviluppo del pensiero computazionale./
+ Strumenti di *elevata qualità scientifica* che _non richiedono_ *preparazione scientifica*.


** Partner e sponsor a vario livello

+ Confindustra Digitale
+ iab Italia (Interactive Advertising Bureau), la più grossa associazione nel campo della pubblicità digitale.
+ TIM
+ Engineering, leader nei servizi IT
+ Cisco, CA technologies, Dea Scuola, SeeWeb, anp (Ass. Nazionale Dirigenti ed alte professionalità nella scuola), Andinf (Ass. Naz. docenti informatica scuola superiore).
+ Intel, HP, Facebook, Microsoft, Samsung, Oracle (2015/16).

** Nessun Dubbio?

** Tutta farina del nostro sacco?

+ la *fuffa* sì ma il resto no.
+ I contributi sono recuperati ed in parte tradotti da /code.org/.
+ /Programma il futuro/ erra ([[http://www.programmailfuturo.it/progetto/cose-il-pensiero-computazionale][http://www.programmailfuturo.it/progetto/cose-il-pensiero-computazionale]]) nell'attribuire l'espressione /computational thinking/ alla *Wing* (2006) e dovrebbe attribuirlo invece a *Papert* (1980).
+ Era giusto per dire...

** Ultima cosa su Programma il Futuro

+ Invita i ragazzi ad iscriversi ([[http://www.programmailfuturo.it/media/docs/diffusione/Volantino-PIF-Secondaria.pdf][Volantino-PIF-Secondaria.pdf]], [[http://www.programmailfuturo.it/media/docs/diffusione/Volantino-PIF-Primaria.pdf][Volantino-PIF-Primaria.pdf]]) al sito web del progetto.
+ Dimentica che secondo l'articolo 2 del Codice Civile i minori _non hanno libertà di agire_.
+ Un minore non potrebbe iscriversi a nessun sito web che comporti l'accettazione di una licenza.
+ Le licenze software sono equiparate ai contratti e quindi un minore non potrebbe sottoscriverle.

* code.org

+ [[http://code.org/][code.org]] in linea di principio è una buona iniziativa.
+ I suoi scopi vanno oltre il mero *coding* e si rivolgono anche alla integrazione razziale o a colmare il /gap tecnologico/ tra i sessi.
+ *Sponsor*: /Mark Zuckerberg and Priscilla Chan, Bill and Melinda Gates Foundation, Quadrivium Foundation, Sean N. Parker Foundation, BlackRock, Salesforce, Drew Houston, Verizon, Reid Hoffman, Diane Tang and Ben Smith, John and Ann Doerr, Bill Gates, Infosys Foundation USA, Ali and Hadi Partovi, Google, Microsoft, Omidyar Network, Ballmer Family Giving, Jeff Bezos/.
+ [[https://italia.code.org/][italia.code.org]] ci dice subito che *2.253.762* italiani hanno partecipato all'ora del codice e che /chiunque può imparare/ (10/10/2016 17:20).
+ [[https://en.wikipedia.org/wiki/Code.org][https://en.wikipedia.org/wiki/Code.org]]

** Fondatore principale

+ Fondata da Hadi Partovi ([[https://en.wikipedia.org/wiki/Hadi_Partovi][https://en.wikipedia.org/wiki/Hadi_Partovi]]), Ali Partovi ([[http://www.partovi.org][http://www.partovi.org]]) ed altri.
+ Hadi Partovi (born 1972) is an Iranian-American businessperson. He has co-founded companies including Tellme Networks and iLike. Additionally, he was the group program manager for Internet Explorer as well as general manager of MSN at Microsoft, and later served as a Senior Vice President at MySpace. Partovi is an angel investor, as well as CEO and co-founder of the non-profit organization Code.org.

** commenti entusiasti 1 ([[https://italia.code.org][italia.code.org]])

+ /Ho visto fuochi d'artificio accendersi sulle teste dei miei studenti, non lampadine./ - *Insegnante*
+ /Il miglior prodotto educativo abbia mai visto./ - *Genitore e imprenditore*
+ /Non ho MAI visto i miei studenti così entusiasti di imparare./ - *Insegnante*
+ /Una delle due cose migliori accadute nel 2013./ - *Studente di seconda*
+ /E poi sapevo che questa sarebbe stata una possibilità di quelle che capitano una sola volta nella vita./ - *Studentessa di quinta*

** commenti entusiasti 2 ([[https://italia.code.org][italia.code.org]])

+ /Adesso sto cercando di convincere altre scuole ad insegnare l'Ora del Codice perché è davvero un'esperienza meravigliosa./ - *Studente di quinta*
+ /Gli studenti erano entusiasti e completamente presi./ - *Insegnante*
+ /Oggi sono entrati di corsa nell'aula prima dell'inizio delle lezioni e mi hanno chiesto se potevano già iniziare. Sono rimasto sbigottito del loro entusiasmo e della loro curiosità per l'Ora del Codice./ - *Insegnante*
+ /I miei tre figli son tornati a casa dalla scuola ieri gridando di entusiasmo per l'Ora del Codice. Quella di 6 anni mi ha spiegato come programmare Angry Birds, mentre quello di 10 proclamava "Da grande farò l'ingegnere informatico. E' il lavoro che fa per me. E' il mio DESTINO!/" - *Genitore*

** commenti meno entusiasti

« I've seen things you people wouldn't believe,  \\
attack ships on fire off the shoulder of Orion,  \\
I watched c-beams glitter in the dark near the Tannhäuser Gates.  \\
All those moments will be lost in time,  \\
like tears in rain.  \\
Time to die. »  \\
(*Roy Batty* - /Blade Runner, Ridley Scott 1982/)

* Scratch forever or only for first?

+ MIT Media Lab's Lifelong Kindergarten group con a capo Resnick sviluppa Scratch nel 2003
+ Idee sulla programmazione visuale della metà degli anni novanta.
+ Linguaggio limitato, manca di strutture dei linguaggi di programmazione reali.
+ Blocchi colorati drag and drop.

** 2003/2013
+ Sviluppato in Squeak/Smalltalk.
+ Multipiattaforma (Microsoft Windows^tm, McOSX^tm, Linux, ...)
+ Realmente open source (Squeak è Open Source) [[https://github.com/LLK/Scratch_1.4][https://github.com/LLK/Scratch_1.4]].
+ Espandibile ed ispezionabile a runtime.

** 2013/oggi
+ Sviluppato in ActionScript.
+ Codice reperibile su github: [[https://github.com/LLK/scratch-flash][https://github.com/LLK/scratch-flash]]
+ Il compilatore Adobe Flex è un progetto Apache ([[https://flex.apache.org/][https://flex.apache.org/]]).
+ Il runtime Adobe Flash/Air è *software proprietario*.
+ Adobe Flash e Adobe Air non sono più supportati in Linux (Adobe ha annunciato di voler rinnovare il supporto in data da destinarsi).

** Pregi

+ Ha favorito un movimento che ha colmato il tempo perso tra Seymour A. Papert (/Mindstorms: Children, Computers, And Powerful Ideas/ - 1980) e la Jeannette Wing (/Computational Thinking/ - 2006).
+ Adatto a ragazzini la cui capacità di scrivere è limitata.
+ Analogia con i ben noti mattoncini Lego^tm.
+ Soddisfazione immediata per studenti ed insegnanti.
+ Ce ne sono altri?

** Difetti

+ Scratch 2 non è un software da considerarsi Open Source: il runtime è proprietario.
+ Linguaggio di programmazione limitato e di tipo imperativo.
+ Non esistono funzioni ma solo procedure, ricorsione limitata.
+ Frustrante e caotico su problemi appena poco complessi.
+ Favorisce il [[http://minimalprocedure.pragmas.org/writings/Coding/coding.html#orgheadline8][Programming by example]].
+ Troppi strumenti di disegno integrati e ampia libreria di sprite.
+ Non stimola l'utilizzo di altri software. 
+ Approccio prevalentemente ludico e modello didattico a /tutor/.

** Studi sulla effettiva validità di Scratch

+ Ci sono molti studi che analizzano la effettiva validità dell'approccio visuale /Scratch-like/.
+ Molti discutono sull'effettiva validità e necessità del /pensiero computazionale/.
+ Alcuni si interrogano su cosa sia davvero il /pensiero computazionale/.
+ In Italia, il MIUR è verbo con i suoi /evangelist, guru e angel investor./

*** Le strutture logiche di programmazione non vengono generalmente usate in Scratch
+ [[http://swerl.tudelft.nl/twiki/pub/Main/TechnicalReports/TUD-SERG-2016-016.pdf][Do Code Smells Hamper Novice Programming? - (Efthimia Aivaloglou, Felienne Hermans)]]
+ /We find that  _procedures  and  conditional loops  are  not  commonly used_.  We further investigate the presence of code smells, including code duplication, dead code, long method and large class  smells.   Our  findings  indicate  that  Scratch  programs _suffer from code smells and especially from dead code and code duplication_./

*** La progressione delle capacità è limitata e presto gli studenti si annoiano
+ [[http://web.engr.oregonstate.edu/~scaffidc/papers/eu_20110607_scrusrprog.pdf][Skill progression demonstrated by users in the scratch animation environment - (Christopher Scaffidi, Christopher Chambers)]]
+ /What  we  found  was  a  _positive  progression  of  social  skills_ (Section 5.1)  alongside  a _negative  progression  of  demonstrated  technical  expertise_,  as  reflected  in a  wide  range of programming  primitives  (Sections 5.2  and 5.3)./ 

*** La risposta del MIT allo studio precedente, che però si pone il dubbio
+ [[https://llk.media.mit.edu/papers/matias-skill_progression_CHI2016.pdf][Skill Progression in Scratch Revisited - (J. Nathan Matias e Sayamindu Dasgupta, MIT; Benjamin Mako Hill)]]
+ /Although Scaffidi and Chambers’s surprising results on depth and breadth seem to have been driven by a _small dataset and an unlucky sample_, their paper includes many other findings and detailed analyses that _remain important for researchers of learning and programming in informal environments_./

*** Scratch non causa nessuna significativa differenza nella capacità di risolvere i problemi nei ragazzini
+ [[https://www.academia.edu/6987699/The_Effects_of_Teaching_Programming_via_Scratch_on_Problem_Solving_Skills_A_Discussion_from_Learners_Perspective][The Effects of Teaching Programming via Scratch on Problem Solving Skills: A Discussion from Learners’ Perspective - (Filiz KALELIOĞLU, Yasemin GÜLBAHAR)]]
+ /According to the quantitative results, programming in Scratch platform _did not cause any signicant differences in the problem solving skills_ of the primary school students. This result may show that programming in Scratch platform may not have an impact on their problem solving skills./

*** Una analisi della quantità di documenti pro o contro le attività computazionali.
+ [[https://www.researchgate.net/publication/303943002_A_Framework_for_Computational_Thinking_Based_on_a_Systematic_Research_Review][A Framework for Computational Thinking Based on a Systematic Research Review - (Filiz Kalelioglu, Volkan Kukul, Yasemin Gulbahar)]]
+ /... *125* examined papers (...). *43* papers were about integration and discussion of courses or activities and CT in the curriculum. *34* papers discussed unplugged methods or computational activities (...) in order to promote and teach the learning of CT. *26* papers defined and criticised computation or CT in order to understand the notion of CT. *24* papers were found to describe an innovative educational system, design or module designed to engage students with CT concepts. Finally, *13* papers were about presenting a pedagogical framework and *four* were about CT pattern analysis./


** Alternative a Scratch

*** Snap!

+ Snap! ([[http://snap.berkeley.edu/][http://snap.berkeley.edu/]]) per rimanere nei cloni, ex nome BYOB.
+ Ha tutte le strutture di un linguaggio di programmazione.
+ Programmato in javascript è espandibile ed ispezionabile.

*** phratch

+ phratch ([[http://www.phratch.com/][http://www.phratch.com/]]) è un ulteriore clone.
+ Deriva dal codice originale di Scratch in Smalltalk.
+ Aggiunge i blocchi di BYOB
+ Multipiattaforma, eseguito su Pharo una moderna piattaforma Open Source in Smalltalk.

*** Beetle Blocks

+ Beetle Blocks ([[http://beetleblocks.com/][http://beetleblocks.com/]]) è basato su Snap!.
+ Progettato per la programmazione 3D.
+ Javascript

*** Blockly

+ Blockly ([[https://developers.google.com/blockly/][https://developers.google.com/blockly/]]) è un progetto Google.
+ Utilizzato dalla maggior parte delle piattaforme di tendenza.
+ Code.org (e quindi programmailfuturo.it), [[http://ai2.appinventor.mit.edu/][App Inventor]], [[https://www.open-roberta.org/en/welcome/][Open Roberta]] e molti altri.
+ Blockly lo utilizzano in molti senza saperlo.

*** Microsoft Research Touch Develop

+ Touch Develop ([[https://www.touchdevelop.com/][https://www.touchdevelop.com/]]) è un ambiente ibrido.
+ Editor semi strutturato con tre livelli di competenze:
  + *principiante* a blocchi
  + *programmatore* con codice semplificato
  + *esperto* con codice /simil-javascript/
+ TypeScript

*** Alice

+ Alice ([[http://www.alice.org/index.php][http://www.alice.org/index.php]]) è una piattaforma visuale.
+ Il codice è affrontabile a più livelli: visuale e testuale.
+ Programmato in Java.
+ Ambiente 3D.

*** Hackety Hack

+ Hackety Hack ([[http://hackety.com/][http://hackety.com/]]) è un ambiente testuale.
+ Sviluppato in Ruby ed utilizza Ruby.
+ Il link non è attualmente raggiungibile.

*** Kids Ruby

+ Kids Ruby ([[http://kidsruby.com/][http://kidsruby.com/]]) è una evoluzione di Hackety Hack.
+ Come il suo predecessore utilizza Ruby.
+ Lo sviluppo è fermo al 2014, ma è usabile.
+ Installatori per Microsoft Windows^tm, Mac OSX^tm, Debian Linux, Raspberry Pi.

*** Ruby e basta

+ Perché?
+ Ruby è divertente
+ [[http://minimalprocedure.pragmas.org/writings/programmazione_elementare_ruby/corso_elementare_ruby.html][Programmazione elementare in Ruby.]]

*** EToys

+ Etoys ([[http://www.squeakland.org/][http://www.squeakland.org/]]) è un ambiente visuale.
+ Appoggiato sulla piattaforma Squeak/Smalltalk.
+ Adatto a bambini di varie età.

*** LOGO

+ LibreLogo ([[http://librelogo.org/en/][http://librelogo.org/en/]]), una implementazione LOGO su LibreOffice.
+ Il LOGO, all'origine del concetto (Papert - 1980) del pensiero computazionale e del costruzionismo.

*** Kojo

+ Kojo ([[http://www.kogics.net/sf:kojo][http://www.kogics.net/sf:kojo]]) un ambiente testuale.
+ Sviluppato in Scala (un linguaggio funzionale) ed usa Scala.
+ Unisce le idee del LOGO, di Processing ([[https://www.processing.org/][https://www.processing.org/]]) e di The Geometer's Sketchpad ([[http://www.dynamicgeometry.com/][http://www.dynamicgeometry.com/]])
+ Lo vedremo dopo.

*** Altri

+ ...
+ Questi ambienti spuntano come funghi.

* Il pensiero computazionale

+ Da [[http://www.programmailfuturo.it/progetto/cose-il-pensiero-computazionale][programmailfuturo.it]]: /L'essenza del concetto, esemplificata magistralmente da questo video, è che con il pensiero computazionale si definiscono procedure che vengono poi attuate da un esecutore, che opera nell'ambito di un contesto prefissato, per raggiungere degli obiettivi assegnati. Il pensiero computazionale è un processo mentale per la risoluzione di problemi costituito dalla combinazione di metodi caratteristici e di strumenti intellettuali, entrambi di valore generale./
+ Per rispondere a tono: ... e quindi?
+ La discussione su cosa sia il /pensiero computazionale/ e sulla sua validità è fonte, come visto, di grosse discussioni all'estero ma non in Italia.

** Computational Thinking, una definizione

+ Computational thinking is the /thought processes/ involved in formulating problems and expressing its solution as transformations to information that an agent can effectively carry out. [Cuny, Snyder, Wing]
+ [[http://research.microsoft.com/en-us/um/redmond/events/asiafacsum2012/day1/Jeannette_Wing.pdf][http://research.microsoft.com/en-us/um/redmond/events/asiafacsum2012/day1/Jeannette_Wing.pdf]]

* Il Coding

+ Il /coding/ ([[http://codemooc.org/un-modello-per-il-coding-a-scuola/][un-modello-per-il-coding-a-scuola/]]) come è inteso può sviluppare il pensiero computazionale?
+ Il *curriculum in computing* pare debba essere ispirato a quello inglese. ([[https://www.gov.uk/government/publications/national-curriculum-in-england-computing-programmes-of-study/national-curriculum-in-england-computing-programmes-of-study][national-curriculum-in-england-computing-programmes-of-study]])
+ Al *Key stage 2* dice
  + /use sequence, selection, and repetition in programs; work with variables and various forms of input and output./
+ Al *Key stage 3* dice
  + /use 2 or more programming languages, at _least one of which is textual_, to solve a variety of computational problems/.
+ Non credo si riferisca a *Scratch*, ma forse noi vogliamo fermarci al *Key stage 1*.

** MOOC

+ I docenti oggi si formano prevalentemente con dei corsi online di poche ore (MOOC).
+ Maestre /alla porta della pensione/ che improvvisamente vedono la luce ed illuminano i loro studenti.
+ Nessuno si chiede se serva una vera preparazione per poter insegnare il pensiero computazionale.
+ Nessuno insegnerebbe italiano o storia o matematica senza saperla, ma il coding si.
+ Qualcosa non torna...

* Il gioco ed il pensiero computazionale

+ Il /gioco/ sviluppa il pensiero computazionale?
+ Il /gioco/ guidato (/tutoring/) stimola il ragazzo a pensare?
+ Impilare centinaia di blocchetti colorati con logiche contorte serve davvero?
+ Ci sono persone che rispondono *no* o perlomeno si interrogano, altre fanno i raduni.

** Giocare quindi non serve a niente?

+ Non è così semplice, *la dimensione giocosa è importante* specialmente con i ragazzini.
+ Se un ragazzo sa scrivere dovrebbe farlo o perlomeno usare un ambiente ibrido.
+ _Scrivere_ porta a _rileggere_ e rileggere porta a _comprendere_, comprendere porta a _migliorare_.
+ Il ragazzo deve comprendere che pensare è anche /fatica/ e non solo /gioco/.
+ Non servono persone in grado di _assemblare_ ma servono persone in grado di _pensare_.

* Linguaggi di programmazione

+ Sono come i linguaggi naturali e modellano le strutture mentali.
+ Ogni paradigma ha un suo diverso approccio al dominio del problema.
+ Alcuni paradigmi sono, a mio parere, migliori di altri nello stimolare il pensiero e l'astrazione.
+ Se ne dovrebbero affrontare più di uno e con paradigmi diversi.

** Il paradigma funzionale ed il pensiero computazionale

+ Papert era un matematico ed il Logo un dialetto LISP.
+ La Wing ha studiato il /lambda calculus/ come dichiara: /...we learned lambda calculus. And I was just blown away! I said, "Wow! This is something I've never seen before and I really like it." (...) And so then, I decided to switch from majoring in electrical engineering to majoring in computer science/. ([[http://www.women.cs.cmu.edu/What/Interviews/jeannetteWing.php][http://www.women.cs.cmu.edu/What/Interviews/jeannetteWing.php]])
+ La Wing a pagina 3 del documento [[http://research.microsoft.com/en-us/um/redmond/events/asiafacsum2012/day1/Jeannette_Wing.pdf][http://research.microsoft.com/en-us/um/redmond/events/asiafacsum2012/day1/Jeannette_Wing.pdf]] cita espressamente: ML e Haskell, insieme a Java e Python.

** l'approccio funzionale

+ Il processo di astrazione e di comprensione dei problemi viene maggiormente stimolato dall'approccio funzionale.
+ Rapporto concettuale diretto tra matematica e linguaggio funzionale.
+ Trasformazione invece che mera computazione.
+ Composizione.
+ Generalizzazione.
+ Elaborazione parallela.
+ Immutabilità.
+ Tipizzazione.

** Linguaggi funzionali puri ed impuri

+ Clojure, Racket/Scheme, Common LISP, LOGO e altri dialetti LISP
+ Haskell, Clean, Curry, Agda, Miranda
+ OCaml, F#, Alice, Standard ML, Coq e altri dialetti ML
+ Erlang, Elixir, Scala, R, Rust
+ Nemerle, Dylan, Idris, Mozart/Oz
+ ...

* Kojo e Scala

+ [[http://www.kogics.net/sf:kojo][http://www.kogics.net/sf:kojo]]
+ *Open Source*
+ Documentato ([[http://www.kogics.net/kojo-ebooks][http://www.kogics.net/kojo-ebooks]])
+ Disponibile in italiano (tartaruga) ([[http://minimalprocedure.pragmas.org/writings/kojo-italiano-doc/kojo-it-doc.html][Funzioni di Kojo in italiano]]).
+ Usato in molti corsi in India, USA, Inghilterra, Svezia.
+ Italia ([[http://minimalprocedure.pragmas.org/writings/kojo-scala-appunti/kojo-scala-appunti.html][Appunti su Scala e Kojo.]]).

** Un ambiente dove attraverso la programmazione si può esplorare

+ Matematica.
+ Arte.
+ Musica.
+ Animazione.
+ Giochi.
+ Arduino, RaspberryPi.
+ Il limite è la fantasia.

** Caratteristiche

+ Grafica LOGO (tartaruga).
+ Tracciatura dell'esecuzione del programma.
+ Supporto per le immagini.
+ Laboratorio Matematico basato su Geogebra.
+ Possibilità di costruire delle *storie* (Storytelling).
+ Composizione musicale.
+ Multilingua e Multipiattaforma.
+ Open Source e GPL
+ ...

** La programmazione

+ Programmazione /punta e clicca/ per iniziare.
+ Supporto completo per la programmazione testuale in Scala.
+ Potente REPL (Read Eval and Print Loop).
+ Sintassi colorata ed autocompletamento del codice.
+ Ispezione degli oggetti.
+ Classpath JAVA personalizzabile per usare qualunque libreria JAVA.
+ Salvataggio e caricamento degli script.
+ ...

** Scala

+ [[http://scala-lang.org/][http://scala-lang.org/]]
+ Moderno, Open Source e potente linguaggio funzionale e multiparadigma.
+ /a language with a low floor, an high ceiling, and wide walls/
+ Il compilatore emette bytecode per la JVM.
+ Usato da: Twitter, Linkedin, Foursquare, Coursera, Apple Inc, The Guardian, New York Times, Huffington Post, USB Bank, BitGold, Meetup, Remember the Milk, Verizon, AirBnB, Zalando, Databrick, Morgan Stanley, Google/Alphabet (Firebase), Unicredit, ...


** Schermate di Kojo

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo1.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo2.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo3.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo4.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo5.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo6.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo7.png]]

*** 
#+ATTR_HTML: :class kojo_screenshot
[[file:./images/kojo8.png]]

*** Documenti su Kojo

+ [[http://minimalprocedure.pragmas.org/writings/kojo-italiano-doc/kojo-it-doc.html][Funzioni di Kojo in italiano]]
+ [[http://minimalprocedure.pragmas.org/writings/kojo-scala-appunti/kojo-scala-appunti.html][Appunti su Scala e Kojo.]]
+ [[https://bitbucket.org/bjornregnell/scaboo/raw/3744f6b18c582e36f8a726173738091b4e6cd6b4/kojobook/tex/book-it.pdf][Challenges with Kojo (Sfide con Kojo) - Björn Regnell, Lund University, 2015]]
+ [[http://www.kogics.net/kojo-ebooks][Kojo Ebooks - Lalit Pant]]

* Conclusioni

+ ... e quindi?

* Contatti e ringraziamenti.

+ Ringrazio lo GNU/Linux User group di Perugia per avermi ospitato.
+ Ringrazio gli ascoltatori per avermi sopportato.
+ Scrivetemi pure a massimo.ghisalberti@gmail.com o massimo.ghisalberti@pragmas.org.
+ [[http://minimalprocedure.pragmas.org/][http://minimalprocedure.pragmas.org/]]
+ *github*: [[https://github.com/minimalprocedure/linux_day_2016][https://github.com/minimalprocedure/linux_day_2016]]
