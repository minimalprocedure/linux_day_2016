Coding, ovvero il delirio computazionale
================================================================================

Slides for the Italian Linux Day 2016 in Magione (GNU/Linux User Group Perugia)

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-nc-sa/4.0/

